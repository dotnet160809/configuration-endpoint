using GB.ConfigurationEndpoint.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace GB.ConfigurationEndpoint;

/// <summary>
/// Extends an <see cref="IApplicationBuilder"/> implementation
/// </summary>
public static class IApplicationBuilderExtension
{
    /// <summary>
    /// Add an endpoint to display the current application configuration
    /// </summary>
    /// <param name="app">The extended <see cref="IApplicationBuilder"/> implementation</param>
    /// <remarks>
    /// The default endpoint is '/configuration'
    /// </remarks>
    /// <returns>The extended <see cref="IApplicationBuilder"/> implementation</returns>
    public static IApplicationBuilder UseConfigurationEndpoint(this IApplicationBuilder app)
    {
        return app.UseConfigurationEndpoint("configuration");
    }

    /// <summary>
    /// Add and endpoint to display the current application configuration
    /// </summary>
    /// <param name="app">the extended <see cref="IApplicationBuilder"/> implementation</param>
    /// <param name="pattern">The URL path where to display the current application configuration</param>
    /// <returns>The extended <see cref="IApplicationBuilder"/> implementation</returns>
    public static IApplicationBuilder UseConfigurationEndpoint(this IApplicationBuilder app, string pattern)
    {
        return app.UseEndpoints(endpoints =>
        {
            endpoints.MapGet(pattern, MapConfigurationEndpoint);
        });
    }

    private static Task MapConfigurationEndpoint(
        HttpContext context,
        [FromServices] IOptionsMonitor<ConfigurationEndpointOptions> configurationOptions,
        [FromServices] IConfiguration configuration,
        [FromServices] IOptionsMonitor<JsonOptions> jsonOptions)
    {
        if (configurationOptions.CurrentValue.IsDisabled || configuration is not IConfigurationRoot root)
        {
            context.Response.StatusCode = StatusCodes.Status404NotFound;

            return Task.CompletedTask;
        }

        context.Response.StatusCode = StatusCodes.Status200OK;
        context.Response.WriteAsJsonAsync(root.Print(), jsonOptions.CurrentValue.JsonSerializerOptions);
        
        return Task.CompletedTask;
    }
}