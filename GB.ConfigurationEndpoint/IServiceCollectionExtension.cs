using Microsoft.Extensions.DependencyInjection;

namespace GB.ConfigurationEndpoint;

/// <summary>
/// Extends an <see cref="IServiceCollection"/> implementation
/// </summary>
public static class IServiceCollectionExtension
{
    /// <summary>
    /// Add the required configuration to add an endpoint displaying the current application configuration
    /// </summary>
    /// <param name="services">The extended <see cref="IServiceCollection"/> implementation</param>
    /// <remarks>
    /// The default configuration section path is 'ConfigurationEndpoint'
    /// </remarks>
    /// <returns>The extended <see cref="IServiceCollection"/> implementation</returns>
    public static IServiceCollection AddConfigurationEndpoint(this IServiceCollection services)
    {
        return services.AddConfigurationEndpoint("ConfigurationEndpoint");
    }

    /// <summary>
    /// Add the required configuration to add an endpoint displaying the current application configuration
    /// </summary>
    /// <param name="services">The extended <see cref="IServiceCollection"/> implementation</param>
    /// <param name="configurationSectionPath">The configuration section path to use when creating the endpoint</param>
    /// <returns>The extended <see cref="IServiceCollection"/> implementation</returns>
    public static IServiceCollection AddConfigurationEndpoint(
        this IServiceCollection services,
        string configurationSectionPath)
    {
        return services
            .AddOptions<ConfigurationEndpointOptions>()
            .BindConfiguration(configurationSectionPath)
            .Services;
    }

    /// <summary>
    /// Add the required configuration to add an endpoint displaying the current application configuration
    /// </summary>
    /// <param name="services">The extended <see cref="IServiceCollection"/> implementation</param>
    /// <param name="optionsBuilder">The <see cref="Action{T}"/> lambda to execute when configuring the required options</param>
    /// <returns>The extended <see cref="IServiceCollection"/> services</returns>
    public static IServiceCollection AddConfigurationEndpoint(
        this IServiceCollection services,
        Action<ConfigurationEndpointOptions> optionsBuilder)
    {
        return services
            .AddOptions<ConfigurationEndpointOptions>()
            .Configure(optionsBuilder)
            .Services;
    }
}