using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using GB.ConfigurationEndpoint.Tests.Assertions;
using Xunit;

namespace GB.ConfigurationEndpoint.Tests;

public sealed class ConfigurationEndpointShould
{
    [Theory]
    [InlineData(false, HttpStatusCode.NotFound)]
    [InlineData(true, HttpStatusCode.OK)]
    public async Task ReturnExpectedStatusCodeBasedOnDefaultConfiguration(bool enable, HttpStatusCode expectedStatusCode)
    {
        // Arrange
        var server = new TestServerBuilder()
            .WithDefaultConfigurationSectionPath()
            .WithConfigurationEndpointEnabled(enable)
            .Build();
        
        // Act
        var response = await server
            .CreateClient()
            .GetAsync("/configuration");
        
        // Assert
        response
            .Should()
            .HaveStatusCode(expectedStatusCode);
    }

    [Theory]
    [InlineData(false, HttpStatusCode.NotFound)]
    [InlineData(true, HttpStatusCode.OK)]
    public async Task ReturnExpectedStatusCodeBasedOnCustomConfiguration(bool enable, HttpStatusCode expectedStatusCode)
    {
        // Arrange
        const string configurationSectionPath = "Custom";
        var server = new TestServerBuilder()
            .WithCustomConfigurationSectionPath(configurationSectionPath)
            .WithConfigurationEndpointEnabled(enable)
            .Build();
        
        // Act
        var response = await server
            .CreateClient()
            .GetAsync("/configuration");
        
        // Assert
        response
            .Should()
            .HaveStatusCode(expectedStatusCode);
    }

    [Theory]
    [InlineData(false, HttpStatusCode.NotFound)]
    [InlineData(true, HttpStatusCode.OK)]
    public async Task ReturnExpectedStatusCodeBasedOnConfigurationWithCustomEndpoint(bool enable, HttpStatusCode expectedStatusCode)
    {
        // Arrange
        const string pattern = "other-configuration";
        var server = new TestServerBuilder()
            .WithDefaultConfigurationSectionPath()
            .WithConfigurationEndpointEnabled(enable)
            .WithConfigurationEndpointPattern(pattern)
            .Build();
        
        // Act
        var response = await server
            .CreateClient()
            .GetAsync(pattern);
        
        // Assert
        response
            .Should()
            .HaveStatusCode(expectedStatusCode);
    }

    [Theory]
    [InlineData("Test", HttpStatusCode.OK)]
    [InlineData("Development", HttpStatusCode.OK)]
    [InlineData("Staging", HttpStatusCode.OK)]
    [InlineData("PreProduction", HttpStatusCode.OK)]
    [InlineData("Production", HttpStatusCode.NotFound)]
    public async Task ReturnExpectedStatusCodeBasedOnProvidedOptionsBuilder(
        string environment,
        HttpStatusCode expectedStatusCode)
    {
        // Arrange
        var server = new TestServerBuilder()
            .WithConfigurationEndpointOptionsBuilder(optionsBuilder =>
            {
                optionsBuilder.Enable = !environment.Equals("Production");
            })
            .Build();
        
        // Act
        var response = await server
            .CreateClient()
            .GetAsync("/configuration");
        
        // Assert
        response
            .Should()
            .HaveStatusCode(expectedStatusCode);
    }

    [Fact]
    public async Task ReturnExpectedConfiguration()
    {
        // Arrange
        var server = new TestServerBuilder()
            .WithDefaultConfigurationSectionPath()
            .WithConfigurationEndpointEnabled(true)
            .Build();
        
        // Act
        var response = await server
            .CreateClient()
            .GetAsync("/configuration");
        
        // Assert
        await ConfigurationEndpointAssertion.Assert(response);
    }
}