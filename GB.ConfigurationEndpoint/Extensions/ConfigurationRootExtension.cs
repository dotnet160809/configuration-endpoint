using Microsoft.Extensions.Configuration;

namespace GB.ConfigurationEndpoint.Extensions;

internal static class ConfigurationRootExtension
{
    public static Dictionary<string, object> Print(this IConfigurationRoot root)
    {
        return root
            .GetChildren()
            .Print(root.Providers);
    }

    private static Dictionary<string, object> Print(
        this IEnumerable<IConfigurationSection> sections,
        IEnumerable<IConfigurationProvider> providers)
    {
        var result = new Dictionary<string, object>();

        foreach (var section in sections)
        {
            if (string.IsNullOrEmpty(section.Key) || string.IsNullOrEmpty(section.Path))
            {
                continue;
            }

            result.Add(section.Key, section.Print(providers));
        }

        return result;
    }

    private static Dictionary<string, object> Print(
        this IConfigurationSection section,
        IEnumerable<IConfigurationProvider> providers)
    {
        var result = new Dictionary<string, object>();

        if (section.GetChildren().Any())
        {
            return section
                .GetChildren()
                .Print(providers);
        }

        foreach (var provider in providers.Reverse())
        {
            if (provider.TryGet(section.Path, out _))
            {
                return new()
                {
                    { "Value", section.Value },
                    { "Provider", provider.ToString() }
                };
            }
        }

        return result;
    }
}