using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;

namespace GB.ConfigurationEndpoint.Tests;

public sealed class TestServerBuilder
{
    private string? _configurationSectionPath;
    private bool _enable;
    private string _pattern = "/configuration";
    private Action<ConfigurationEndpointOptions>? _optionsBuilder;

    public TestServerBuilder WithCustomConfigurationSectionPath(string configurationSectionPath)
    {
        if (string.IsNullOrEmpty(configurationSectionPath))
        {
            throw new ArgumentNullException(configurationSectionPath);
        }

        _configurationSectionPath = configurationSectionPath;

        return this;
    }

    public TestServerBuilder WithDefaultConfigurationSectionPath()
    {
        return WithCustomConfigurationSectionPath("ConfigurationEndpoint");
    }
    
    public TestServerBuilder WithConfigurationEndpointEnabled(bool enable)
    {
        _enable = enable;

        return this;
    }

    public TestServerBuilder WithConfigurationEndpointOptionsBuilder(Action<ConfigurationEndpointOptions> optionsBuilder)
    {
        _optionsBuilder = optionsBuilder;

        return this;
    }

    public TestServerBuilder WithConfigurationEndpointPattern(string pattern)
    {
        if (string.IsNullOrEmpty(pattern))
        {
            throw new ArgumentNullException(pattern);
        }

        _pattern = pattern;

        return this;
    }

    public TestServer Build()
    {
        return new(CreateWebHostBuilder());
    }

    private IWebHostBuilder CreateWebHostBuilder()
    {
        return new WebHostBuilder()
            .UseSetting($"{_configurationSectionPath}:Enable", _enable.ToString())
            .ConfigureServices(ConfigureServices)
            .Configure(ConfigureApplicationBuilder);
    }

    private void ConfigureServices(IServiceCollection services)
    {
        _ = services.AddRouting();

        if (_optionsBuilder is not null)
        {
            _ = services.AddConfigurationEndpoint(_optionsBuilder);

            return;
        }
        
        if (!string.IsNullOrEmpty(_configurationSectionPath))
        {
            _ = services.AddConfigurationEndpoint(_configurationSectionPath);

            return;
        }

        _ = services.AddConfigurationEndpoint();
    }

    private void ConfigureApplicationBuilder(IApplicationBuilder app)
    {
        _ = app.UseRouting();

        if (!string.IsNullOrEmpty(_pattern))
        {
            _ = app.UseConfigurationEndpoint(_pattern);

            return;
        }
                
        _ = app.UseConfigurationEndpoint();
    }
}