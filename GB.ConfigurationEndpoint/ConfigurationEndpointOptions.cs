namespace GB.ConfigurationEndpoint;

/// <summary>
/// Represents the required options to configuration the endpoint
/// </summary>
public sealed record ConfigurationEndpointOptions
{
    /// <summary>
    /// Indicates if the endpoint displaying the application configuration is enabled
    /// </summary>
    public bool Enable { get; set; }

    internal bool IsDisabled => !Enable;
}