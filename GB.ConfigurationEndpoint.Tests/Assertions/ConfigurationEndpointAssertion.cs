using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using FluentAssertions;

namespace GB.ConfigurationEndpoint.Tests.Assertions;

public static class ConfigurationEndpointAssertion
{
    public static async Task Assert(HttpResponseMessage response)
    {
        response
            .Should()
            .HaveStatusCode(HttpStatusCode.OK);

        await using var stream = await response.Content.ReadAsStreamAsync();
        var document = await JsonDocument.ParseAsync(stream);
        Assert(document);
    }
    
    private static void Assert(JsonDocument document)
    {
        document
            .RootElement
            .TryGetProperty("ConfigurationEndpoint", out var jsonElement)
            .Should()
            .BeTrue();
        jsonElement
            .TryGetProperty("Enable", out var enableJsonElement)
            .Should()
            .BeTrue();
        enableJsonElement
            .TryGetProperty("Value", out var valueJsonElement)
            .Should()
            .BeTrue();
        valueJsonElement
            .GetString()
            .Should()
            .Be("True");
        enableJsonElement
            .TryGetProperty("Provider", out var providerJsonElement)
            .Should()
            .BeTrue();
        providerJsonElement
            .GetString()
            .Should()
            .NotBeNull();
    }
}